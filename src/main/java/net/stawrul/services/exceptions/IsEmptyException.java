package net.stawrul.services.exceptions;

/**
 * Wyjątek sygnalizujący puste zamowienie.
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */

public class IsEmptyException extends RuntimeException {
}
