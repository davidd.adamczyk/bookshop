package net.stawrul;

import net.stawrul.controllers.BooksController;
import net.stawrul.model.Book;
import net.stawrul.services.BooksService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityManager;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BooksServiceTest {

    @Mock
    EntityManager em;

    @Test
    public void whenAddBook_placeStatusCreated() throws URISyntaxException
    {
        // Arrange
        Book book = new Book();
        book.setAmount(10);
        book.setTitle("abc");


        BooksService ordersService = new BooksService(em);
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(null);

        BooksController controller = new BooksController(ordersService);

        UriComponentsBuilder uriBuilder = Mockito.mock(
                UriComponentsBuilder.class,
                RETURNS_DEEP_STUBS
        );


        Mockito.when(
                uriBuilder
                        .path(any())
                        .buildAndExpand(book.getId())
                        .toUri()
        ).thenReturn(
                new URI("/books/17")
        );

        // Act
        ResponseEntity result = controller.addBook(book, uriBuilder);

        // Assert
        Mockito.verify(em, times(1)).persist(book);
        assertEquals(HttpStatus.CREATED, result.getStatusCode());

    }

    @Test
    public void whenAddBookWithNoTitle_placeStatusConflict()
    {
        // Arrange
        Book book = new Book();
        BooksService ordersService = new BooksService(em);
        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);


        BooksController controller = new BooksController(ordersService);

        UriComponentsBuilder uriBuilder = Mockito.mock(
                UriComponentsBuilder.class, //klasa do zamockowania
                RETURNS_DEEP_STUBS //tryb mockowania
        );

        // Act
        ResponseEntity result = controller.addBook(book, uriBuilder);

        // Assert
        assertEquals(HttpStatus.CONFLICT, result.getStatusCode());
    }



    @Test
    public void whenUpdateExistingBook_placeStatusOK()
    {
        // Arrange
        Book book1 = new Book();
        book1.setTitle("abc");
        book1.setAmount(10);

        Book book2 = new Book();
        book2.setTitle("abcd");
        book2.setAmount(13);
        book2.setId(book1.getId());

        BooksService ordersService = new BooksService(em);
        Mockito.when(ordersService.find(book2.getId())).thenReturn(book1);

        BooksController controller = new BooksController(ordersService);

        // Act
        ResponseEntity result = controller.updateBook(book2);

        // Assert
        Mockito.verify(em, times(1)).merge(book2);
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    public void whenUpdateNonExistingBook_placeStatusNotFound()
    {
        // Arrange
        Book book = new Book();

        BooksService ordersService = new BooksService(em);
        Mockito.when(ordersService.find(book.getId())).thenReturn(null);

        BooksController controller = new BooksController(ordersService);

        // Act
        ResponseEntity result = controller.updateBook(book);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }
}
